Node Body Ajax
=============

In the simplest terms, Node Body Ajax module provides tools to loads the node body via AJAX so that it isn't visible when the user does "view source." There is also an option you can turn on that will also attempting to disable copy & paste via Javascript.

After enabling this module, Node body ajax setting menu will show under the Appearance Menu.

Configuration Page
------------------

After the enabling this module, Background menu visible Under the appearance menu (admin/appearance/node_body_ajax). 

On the configuration page (admin/appearance/node_body_ajax), you can configure which type of content use ajax and use disable properties tocopy the content body.

## Features

- Choose content type
- Choose disable selection
- Located in appearance menu

Author:
-------
Madhulata Infotech(Team). 
